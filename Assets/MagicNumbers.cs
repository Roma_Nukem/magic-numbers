﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicNumbers : MonoBehaviour
{
    int min = 1;
    int max = 1000;
    int guess;
    int how_much = 0 ;
    string myName = "Roman";
    // Start is called before the first frame update
    void Start()
    {
        StartGame();
    }

    void StartGame()
    {
        min = 1;
        max = 1000;
        guess = (min + max) / 2;
        Debug.Log("Hello, " + myName + "!");
        Debug.Log("Загадай число от " + min + " до " + max);
        print("Ты загадал число " + guess + "?");
        
    }

    void NewGuess()
    {
        how_much += 1;
        guess = (min + max) / 2;
        Debug.Log("Может это " + guess + "?");
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            min = guess;
            NewGuess();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            max = guess;
            NewGuess();
        }
        else if (Input.GetKeyDown(KeyCode.Return))
        {
            Debug.Log("Я красавчик");
            Debug.Log("Всего ходов: " + how_much);
            StartGame();
        }
    }
}
