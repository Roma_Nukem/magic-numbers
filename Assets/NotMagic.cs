﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotMagic : MonoBehaviour
{
    int amount = 50 ;
    int steps = 0;
    int our_amount = 0;
    // Start is called before the first frame update
    void Start()
    {
        NewGame();
    }
    void Operation()
    {
        if (steps < 3)
        {
            Debug.Log("Жми любые числа от 1 до 9");
        }
        else if (steps >= 3 && steps < 7)
        {
            Debug.Log("О да! Продолжай!");
        }
        else if (steps >= 7)
        {
            Debug.Log("Ты просто лучший!");
        }
        steps += 1;

    }
    void NewGame()
    {
        steps = 0;
        our_amount = 0;
        Debug.Log("Привет!");
        Debug.Log("Жми любые числа от 1 до 9");
    }
    void NewStep()
    {
        Debug.Log("Общая сумма" + our_amount);
        Operation();
    }
    void Zeroing()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            our_amount = 0;
            Operation();
        }
    }
    void Update()
    {
        if (our_amount < amount) 
        {
            if (Input.GetKeyDown(KeyCode.Alpha1)) 
            {
                our_amount += 1;
                NewStep();
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2)) 
            {
                our_amount += 2;
                NewStep();
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                our_amount += 3;
                NewStep();
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                our_amount += 4;
                NewStep();
            }
            else if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                our_amount += 5;
                NewStep();
            }
            else if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                our_amount += 6;
                NewStep();
            }
            else if (Input.GetKeyDown(KeyCode.Alpha7))
            {
                our_amount += 7;
                NewStep();
            }
            else if (Input.GetKeyDown(KeyCode.Alpha8))
            {
                our_amount += 8;
                NewStep();
            }
            else if (Input.GetKeyDown(KeyCode.Alpha9))
            {
                our_amount += 9;
                NewStep();
            }
        }
        else if (our_amount == amount)
        {
            Debug.Log("Да! Ты сделал это! Всего за " + steps + " ходов!");
            NewGame();
        }
        else if (our_amount > amount)
        {
            Debug.Log("Упс! Перебор! Нажми на пробел, чтобы обнулить счетчик.");
            Zeroing();
        }

    }
}
